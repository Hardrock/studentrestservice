package com.epam.brest.studentRestService.dao;

import com.epam.brest.studentRestService.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student, Long> {
}
