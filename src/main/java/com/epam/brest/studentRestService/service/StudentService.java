package com.epam.brest.studentRestService.service;

import com.epam.brest.studentRestService.model.Student;

import java.util.List;
import java.util.Optional;


public interface StudentService {
    List<Student> findAll();
    Optional<Student> findById(Long studentId);
    void addStudent (Student student);
    void deleteStudent(Long studentId);
    void updateStudent(Student student);
}
