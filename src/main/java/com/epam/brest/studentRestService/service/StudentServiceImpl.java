package com.epam.brest.studentRestService.service;

import com.epam.brest.studentRestService.dao.StudentRepository;
import com.epam.brest.studentRestService.model.Student;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
@AllArgsConstructor
@Slf4j
public class StudentServiceImpl implements StudentService{

    private final StudentRepository studentRepository;

    @Override
    public List<Student> findAll() {
        log.debug("findAll()");
        return studentRepository.findAll();
    }

    @Override
    public Optional<Student> findById(Long studentId) {
        log.debug("findById({})", studentId);
        return studentRepository.findById(studentId);
    }

    @Override
    public void addStudent(Student student) {
        log.debug("addStudent({})", student);
        studentRepository.save(student);
    }

    @Override
    public void deleteStudent(Long studentId) {
        log.debug("deleteStudent({})", studentId);
        boolean exist = studentRepository.existsById(studentId);
        if(!exist){
            throw new IllegalArgumentException("Student is not exist");
        }
        studentRepository.deleteById(studentId);
    }

    @Override
    public void updateStudent(Student student) {
        log.debug("updateStudent({})", student);
        Optional<Student> optionalStudent = studentRepository.findById(student.getId());
        if (optionalStudent.isEmpty()){
            throw new IllegalArgumentException("Updated student is not exist");
        }
        Student updatedStudent = optionalStudent.get();
        updatedStudent.setFirstName(student.getFirstName());
        updatedStudent.setLastName(student.getLastName());
    }
}
