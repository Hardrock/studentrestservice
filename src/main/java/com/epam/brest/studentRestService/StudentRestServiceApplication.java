package com.epam.brest.studentRestService;

import com.epam.brest.studentRestService.dao.StudentRepository;
import com.epam.brest.studentRestService.model.Student;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@SpringBootApplication
@Configuration
public class StudentRestServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(StudentRestServiceApplication.class, args);
	}

	@Bean
	CommandLineRunner commandLineRunner(StudentRepository studentRepository){
		return args -> {
			Student student1 = Student.builder()
					.firstName("Kate")
					.lastName("Petrova")
					.build();
			Student student2 = Student.builder()
					.firstName("Denis")
					.lastName("Ivanov")
					.build();
			studentRepository.saveAll(List.of(student1, student2));
		};
	}

}
