package com.epam.brest.studentRestService.security;

import com.google.common.collect.Sets;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Set;
import java.util.stream.Collectors;

import static com.epam.brest.studentRestService.security.ApplicationUserPermission.STUDENT_READ;
import static com.epam.brest.studentRestService.security.ApplicationUserPermission.STUDENT_WRITE;

@AllArgsConstructor
@Getter
public enum ApplicationUserRole {
    ADMIN(Sets.newHashSet(STUDENT_READ, STUDENT_WRITE)),
    STUDENT(Sets.newHashSet(STUDENT_READ)),
    GUEST(Sets.newHashSet());

    private final Set<ApplicationUserPermission> permissions;

    public Set<SimpleGrantedAuthority> getGrantedAuthority(){
        Set<SimpleGrantedAuthority> permissions = getPermissions().stream()
                .map(permission -> new SimpleGrantedAuthority(permission.getPermission()))
                .collect(Collectors.toSet());
        permissions.add(new SimpleGrantedAuthority("ROLE_" + this.name()));
        return permissions;
    }
}
