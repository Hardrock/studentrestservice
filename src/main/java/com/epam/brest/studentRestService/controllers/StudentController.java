package com.epam.brest.studentRestService.controllers;

import com.epam.brest.studentRestService.model.Student;
import com.epam.brest.studentRestService.service.StudentService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/students")
@AllArgsConstructor
@Slf4j
public class StudentController {

    private final StudentService studentService;

    @GetMapping
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_STUDENT', 'ROLE_GUEST')")
    public List<Student> findAll(){
        log.debug("findAll()");
        return studentService.findAll();
    }

    @GetMapping("/{studentId}")
    @PreAuthorize("hasAuthority('student:read')")
    public Student findById(@PathVariable("studentId") Long studentId){
        log.debug("findById({})", studentId);
        return studentService
                .findById(studentId)
                .orElseThrow(() -> new IllegalArgumentException("Student" + studentId + "is not exist"));
    }

    @PostMapping
    @PreAuthorize("hasAuthority('student:write')")
    public void addStudent (@RequestBody Student student){
        log.debug("addStudent({})", student);
        studentService.addStudent(student);
    }

    @DeleteMapping("/{studentId}")
    @PreAuthorize("hasAuthority('student:write')")
    public void deleteStudent(@PathVariable("studentId") Long studentId){
        log.debug("deleteStudent({})", studentId);
        studentService.deleteStudent(studentId);
    }

    @PutMapping
    @PreAuthorize("hasAuthority('student:write')")
    public void updateStudent(@RequestBody Student student){
        log.debug("updateStudent({})", student);
        studentService.updateStudent(student);
    }
}
