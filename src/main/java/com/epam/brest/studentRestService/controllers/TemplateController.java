package com.epam.brest.studentRestService.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
@Slf4j
public class TemplateController {

    @GetMapping("login")
    public String getLoginView () {
        log.debug("getLoginView ()");
        return "login";
    }

    @GetMapping
    public String redirectToStudents() {
        log.debug("redirectToStudents()");
        return "redirect:/students";
    }
}
