package com.epam.brest.studentRestService;

import com.epam.brest.studentRestService.dao.StudentRepository;
import com.epam.brest.studentRestService.model.Student;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.bytebuddy.utility.RandomString;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Example;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@SpringBootTest
@Transactional
class StudentControllerIntegrationTest {

    private static final String STUDENT_ENDPOINT = "/students";

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private StudentRepository studentRepository;

    private MockMvc mockMvc;

    @BeforeEach
    public void setUp (){
        mockMvc = MockMvcBuilders.webAppContextSetup(wac)
                .apply(SecurityMockMvcConfigurers.springSecurity())
                .alwaysDo(print())
                .build();
    }

 /*   @Test
    void findAllShouldReturnStatus401() throws Exception {
        mockMvc.perform(get(STUDENT_ENDPOINT)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized())
                .andReturn();
    }

	@Test
    @WithUserDetails(value = "Don")
	void findAllWithDonShouldReturnCorrectData() throws Exception {

	    Student student1 = studentRepository.findById(1L).orElseThrow();
        Student student2 = studentRepository.findById(2L).orElseThrow();

        mockMvc.perform(get(STUDENT_ENDPOINT)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].firstName").value(student1.getFirstName()))
                .andExpect(jsonPath("$[1].firstName").value(student2.getFirstName()))
                .andReturn();
	}

    @Test
    @WithUserDetails(value = "Anna")
    void findAllWithAnnaShouldReturnCorrectData() throws Exception {

        Student student1 = studentRepository.findById(1L).orElseThrow();
        Student student2 = studentRepository.findById(2L).orElseThrow();

        mockMvc.perform(get(STUDENT_ENDPOINT)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].firstName").value(student1.getFirstName()))
                .andExpect(jsonPath("$[1].firstName").value(student2.getFirstName()))
                .andReturn();
    }

    @Test
    @WithUserDetails(value = "Denis")
    void findAllWithDenisShouldReturnCorrectData() throws Exception {

        Student student1 = studentRepository.findById(1L).orElseThrow();
        Student student2 = studentRepository.findById(2L).orElseThrow();

        mockMvc.perform(get(STUDENT_ENDPOINT)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].firstName").value(student1.getFirstName()))
                .andExpect(jsonPath("$[1].firstName").value(student2.getFirstName()))
                .andReturn();
    }

    @Test
    void findByIdShouldReturnStatus401() throws Exception {
        mockMvc.perform(get(STUDENT_ENDPOINT + "/1")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized())
                .andReturn();
    }

    @Test
    @WithUserDetails("Don")
    void findByIdWithDonShouldReturnStatus403() throws Exception {
        mockMvc.perform(get(STUDENT_ENDPOINT + "/1")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden())
                .andReturn();
    }

	@Test
    @WithUserDetails("Anna")
    void findByIdWithAnnaShouldReturnCorrectData() throws Exception {
        Long studentId = 1L;
        Student student1 = studentRepository.findById(studentId).orElseThrow();

        mockMvc.perform(get(STUDENT_ENDPOINT + "/{studentId}", studentId)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.firstName").value(student1.getFirstName()))
                .andReturn();
    }

    @Test
    @WithUserDetails("Denis")
    void findByIdWithDenisShouldReturnCorrectData() throws Exception {
        Long studentId = 1L;
        Student student1 = studentRepository.findById(studentId).orElseThrow();

        mockMvc.perform(get(STUDENT_ENDPOINT + "/{studentId}", studentId)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.firstName").value(student1.getFirstName()))
                .andReturn();
    }

    @Test
    void addStudentShouldReturnStatus401() throws Exception {
        Student student = Student.builder()
                .firstName(RandomString.make())
                .lastName(RandomString.make())
                .build();
        mockMvc.perform(post(STUDENT_ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(student)))
                .andDo(print())
                .andExpect(status().isUnauthorized())
                .andReturn();
        assertFalse(studentRepository.exists(Example.of(student)));
    }

    @Test
    @WithUserDetails("Don")
    void addStudentWithDonShouldReturnStatus403() throws Exception {
        Student student = Student.builder()
                .firstName(RandomString.make())
                .lastName(RandomString.make())
                .build();
        mockMvc.perform(post(STUDENT_ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(student)))
                .andDo(print())
                .andExpect(status().isForbidden())
                .andReturn();
        assertFalse(studentRepository.exists(Example.of(student)));
    }

    @Test
    @WithUserDetails("Anna")
    void addStudentWithAnnaShouldReturnStatus403() throws Exception {
        Student student = Student.builder()
                .firstName(RandomString.make())
                .lastName(RandomString.make())
                .build();
        mockMvc.perform(post(STUDENT_ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(student)))
                .andDo(print())
                .andExpect(status().isForbidden())
                .andReturn();
        assertFalse(studentRepository.exists(Example.of(student)));
    }

	@Test
    @WithUserDetails("Denis")
    void addStudentWithDenisShouldAddStudent() throws Exception {
	    Student student = Student.builder()
                .firstName(RandomString.make())
                .lastName(RandomString.make())
                .build();
	    mockMvc.perform(post(STUDENT_ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(student)))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();
	    assertTrue(studentRepository.exists(Example.of(student)));
    }

    @Test
    void deleteStudentShouldReturnStatus401() throws Exception {
        Student student = studentRepository.findById(1L).orElseThrow();
        mockMvc.perform(delete(STUDENT_ENDPOINT + "/{studentId}", 1))
                .andDo(print())
                .andExpect(status().isUnauthorized())
                .andReturn();
        assertTrue(studentRepository.exists(Example.of(student)));
    }

    @Test
    @WithUserDetails("Don")
    void deleteStudentWithDonShouldReturnStatus403() throws Exception {
        Student student = studentRepository.findById(1L).orElseThrow();
        mockMvc.perform(delete(STUDENT_ENDPOINT + "/{studentId}", 1))
                .andDo(print())
                .andExpect(status().isForbidden())
                .andReturn();
        assertTrue(studentRepository.exists(Example.of(student)));
    }

    @Test
    @WithUserDetails("Anna")
    void deleteStudentWithAnnaShouldReturnStatus403() throws Exception {
        Student student = studentRepository.findById(1L).orElseThrow();
        mockMvc.perform(delete(STUDENT_ENDPOINT + "/{studentId}", 1))
                .andDo(print())
                .andExpect(status().isForbidden())
                .andReturn();
        assertTrue(studentRepository.exists(Example.of(student)));
    }

    @Test
    @WithUserDetails("Denis")
    void deleteStudentWithDenisShouldDeleteStudent() throws Exception {
	    Student student = studentRepository.findById(1L).orElseThrow();
	    mockMvc.perform(delete(STUDENT_ENDPOINT + "/{studentId}", 1))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();
	    assertFalse(studentRepository.exists(Example.of(student)));
    }

    @Test
    void updateStudentShouldReturnStatus401() throws Exception {

        Student student = new Student();
        student.setId(1L);
        student.setFirstName(RandomString.make());
        student.setLastName(RandomString.make());

        mockMvc.perform(put(STUDENT_ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(student)))
                .andDo(print())
                .andExpect(status().isUnauthorized())
                .andReturn();

        assertNotEquals(
                studentRepository.findById(1L).orElseThrow().getFirstName(),
                student.getFirstName());
        assertNotEquals(
                studentRepository.findById(1L).orElseThrow().getLastName(),
                student.getLastName());
    }

    @Test
    @WithUserDetails("Don")
    void updateStudentWithDonShouldReturnStatus401() throws Exception {

        Student student = new Student();
        student.setId(1L);
        student.setFirstName(RandomString.make());
        student.setLastName(RandomString.make());

        mockMvc.perform(put(STUDENT_ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(student)))
                .andDo(print())
                .andExpect(status().isForbidden())
                .andReturn();

        assertNotEquals(
                studentRepository.findById(1L).orElseThrow().getFirstName(),
                student.getFirstName());
        assertNotEquals(
                studentRepository.findById(1L).orElseThrow().getLastName(),
                student.getLastName());
    }

    @Test
    @WithUserDetails("Anna")
    void updateStudentWithAnnaShouldReturnStatus401() throws Exception {

        Student student = new Student();
        student.setId(1L);
        student.setFirstName(RandomString.make());
        student.setLastName(RandomString.make());

        mockMvc.perform(put(STUDENT_ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(student)))
                .andDo(print())
                .andExpect(status().isForbidden())
                .andReturn();

        assertNotEquals(
                studentRepository.findById(1L).orElseThrow().getFirstName(),
                student.getFirstName());
        assertNotEquals(
                studentRepository.findById(1L).orElseThrow().getLastName(),
                student.getLastName());
    }

    @Test
    @WithUserDetails("Denis")
    void updateStudentWithDenisShouldUpdateStudent() throws Exception {

	    Student student = new Student();
	    student.setId(1L);
	    student.setFirstName(RandomString.make());
	    student.setLastName(RandomString.make());

	    mockMvc.perform(put(STUDENT_ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(student)))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

	    assertEquals(
	            studentRepository.findById(1L).orElseThrow().getFirstName(),
                student.getFirstName());
        assertEquals(
                studentRepository.findById(1L).orElseThrow().getLastName(),
                student.getLastName());
    }*/
}
