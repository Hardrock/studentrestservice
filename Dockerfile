FROM openjdk:11
WORKDIR usr/src/app
COPY build/libs/studentRestService-0.0.1-SNAPSHOT.jar app.jar
COPY wait-for-it.sh wait-for-it.sh
CMD ["java","-jar","app.jar"]
