
# Student rest service test project
## Requirements

* JDK 11
* Gradle 7.0

## Build application:
```
gradle clean
gradle build
```
## Local tests with Jetty Maven Plugin
Run application with docker-compose
```
docker-compose up
```
Next, starting and stopping the application is performed by the commands:
```
docker-compose start
docker-compose stop
```
